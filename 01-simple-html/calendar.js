
function initStagecalCalendar(containerId, url) {
	var request = new XMLHttpRequest();
	request.open('GET', url, true);
	request.onreadystatechange = function() {
		if (this.readyState === 4) {
			if (this.status == 0 || (this.status >= 200 && this.status < 400)) {
				var html = "";
				var data = JSON.parse(this.responseText).data;
				data.forEach(function (item) {
					var date = new Date(item.DateTime).toLocaleString({}, {weekday: 'long',  year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric'});
					var place = item.PlaceName || item.Address || '';
					html += `<p class="stagecal-item"><h1>${item.Name}</h1><h2>${place}</h2><div>${date}</div></p>\n`;
				});
				
				var cont = document.getElementById(containerId);
				cont.innerHTML = html;
				
			} else {
				console.error("Error while getting calendar from Stagecal.", this);
			}
		}
	};
    request.send();
}